use clap::Parser;
use triadic_memory::{
    encoders::{ScalarEncoder, ScalarFloatEncoder},
    sdr,
};

#[derive(Debug, Parser)]
#[clap(author, version, about)]
struct Cli {
    /// Size of the SDRs
    #[clap(long, short = 'N', default_value_t = sdr::SIZE)]
    size: usize,

    /// Number of active connections in the SDRs
    #[clap(long, short = 'P', default_value_t = sdr::POP)]
    pop: usize,

    /// Length of sequence to encode
    #[clap(long, short, default_value_t = 20)]
    length: usize,

    /// If set, encode a linear sequence of values <STEP> apart
    #[clap(long, short, default_value_t = 1.0)]
    step: f64,

    /// Resolution of the encoder
    #[clap(long, short, default_value_t = 1.0)]
    resolution: f64,

    /// Value to begin the sequence
    #[clap(long, default_value_t = 0.0)]
    start: f64,
}

fn main() {
    let cli = Cli::parse();
    let size = cli.size;
    let pop = cli.pop;
    let len = cli.length;
    let step = cli.step;
    let start = cli.start;

    let resolution = cli.resolution;

    let enc = ScalarFloatEncoder::new(size, pop, resolution);

    let mut data = Vec::with_capacity(len);

    for i in 0..len {
        data.push(i as f64 * step + start);
    }

    let mut sdrs = Vec::with_capacity(len);
    for p in data.iter() {
        let s = enc.encode(*p);
        sdrs.push(s);
    }
    let data = data;
    let sdrs = sdrs;

    println!("\nEncoding with {:#?},\nradius: {}\n", &enc, &enc.radius());

    let s0 = &sdrs[0];
    for (i, sdr) in sdrs.iter().enumerate() {
        println!(
            "v: {}, overlap with SDR for {start}: {}",
            &data[i],
            s0.overlap(sdr)
        );
    }
}
