use crate::sdr::{self, Sdr};
use bitvec::prelude::{bitvec, BitVec};
use std::fmt::Debug;

pub type ConWeight = u16;

#[derive(Clone, Copy, Debug)]
enum MemOp {
    Store,
    Erase,
}

pub struct MemoryRequestError(usize);

impl Debug for MemoryRequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "MemoryFetchError: only 1 blank can be requested, but got requests for {}",
            &self.0
        ))
    }
}

#[derive(Clone)]
pub struct TriadicMemory {
    pub weights: BitVec,
    size: usize,
    pop: usize,
}

impl Default for TriadicMemory {
    fn default() -> Self {
        Self::new(sdr::SIZE, sdr::POP)
    }
}

impl Debug for TriadicMemory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut connections = 0;
        for weight in self.weights.iter() {
            if *weight {
                connections += 1;
            }
        }
        f.write_fmt(format_args!(
            "TriadicMemory: {} connections, pop: {}, size: {}",
            connections, self.pop, self.size,
        ))
    }
}

impl TriadicMemory {
    pub fn new(size: usize, pop: usize) -> Self {
        let weights = bitvec![0; size.pow(3)];
        Self { weights, size, pop }
    }

    pub fn store(&mut self, x: &Sdr, y: &Sdr, z: &Sdr) {
        self.adjust_weights(x, y, z, MemOp::Store);
        // TODO: figure out forget()ing
    }

    pub fn erase(&mut self, x: &Sdr, y: &Sdr, z: &Sdr) {
        self.adjust_weights(x, y, z, MemOp::Erase)
    }

    pub fn fetch(
        &self,
        x: Option<&Sdr>,
        y: Option<&Sdr>,
        z: Option<&Sdr>,
    ) -> Result<Sdr, MemoryRequestError> {
        let sdr = match (x, y, z) {
            (None, Some(y), Some(z)) => self.fetch_x(y, z),
            (Some(x), None, Some(z)) => self.fetch_y(x, z),
            (Some(x), Some(y), None) => self.fetch_z(x, y),
            (Some(_), _, _) | (_, Some(_), _) | (_, _, Some(_)) => {
                return Err(MemoryRequestError(2))
            }
            _ => return Err(MemoryRequestError(3)),
        };

        Ok(sdr)
    }

    // private methods down here
    fn fetch_x(&self, y: &Sdr, z: &Sdr) -> Sdr {
        let n = self.size;
        assert!(y.size <= n);
        assert!(z.size <= n);

        let n2 = n.pow(2);
        let mut weights = vec![0; n];

        for &jy in y.connections().iter() {
            for &kz in z.connections().iter() {
                let addr = n * jy as usize + kz as usize;
                for (i, w) in weights.iter_mut().enumerate() {
                    let addr = n2 * i + addr;
                    // SAFETY: we've asserted our addr can't be out of bounds.
                    unsafe {
                        *w += *self.weights.get_unchecked(addr) as ConWeight;
                    }
                }
            }
        }

        Sdr::from_weights(&weights, self.pop)
    }

    fn fetch_y(&self, x: &Sdr, z: &Sdr) -> Sdr {
        let n = self.size;
        assert!(x.size <= n);
        assert!(z.size <= n);

        let n2 = n.pow(2);
        let mut weights = vec![0; n];

        for &ix in x.connections().iter() {
            for &kz in z.connections().iter() {
                let addr = n2 * ix as usize + kz as usize;
                for (j, w) in weights.iter_mut().enumerate() {
                    let addr = addr + n * j;
                    // SAFETY: we've asserted our addr can't be out of bounds.
                    unsafe {
                        *w += *self.weights.get_unchecked(addr) as ConWeight;
                    }
                }
            }
        }

        Sdr::from_weights(&weights, self.pop)
    }

    fn fetch_z(&self, x: &Sdr, y: &Sdr) -> Sdr {
        let n = self.size;
        assert!(y.size <= n);
        assert!(x.size <= n);

        let n2 = n.pow(2);
        let mut weights = vec![0; n];

        for &ix in x.connections().iter() {
            for &jy in y.connections().iter() {
                let addr = n2 * ix as usize + n * jy as usize;
                for (k, w) in weights.iter_mut().enumerate() {
                    let addr = addr + k;
                    // SAFETY: we've asserted our addr can't be out of bounds.
                    unsafe {
                        *w += *self.weights.get_unchecked(addr) as ConWeight;
                    }
                }
            }
        }

        Sdr::from_weights(&weights, self.pop)
    }

    fn adjust_weights(&mut self, x: &Sdr, y: &Sdr, z: &Sdr, adjuster: MemOp) {
        let n = self.size;
        let n2 = n.pow(2);

        for &ix in x.connections().iter() {
            for &jy in y.connections().iter() {
                for &kz in z.connections().iter() {
                    let addr = n2 * ix as usize + n * jy as usize + kz as usize;
                    let mut weight = self.weights.get_mut(addr).unwrap();
                    *weight = match adjuster {
                        MemOp::Store => true,
                        MemOp::Erase => false,
                    };
                }
            }
        }
    }
}
