use self::util::yhash;
use crate::sdr::{AddrType, Sdr};
use std::collections::HashSet;

pub trait ScalarEncoder {
    type Value;

    fn encode(&self, value: Self::Value) -> Sdr;

    /// Values more than or equal to this far apart should not share any bits in their SDRs, but may
    /// by chance
    fn radius(&self) -> Self::Value;
}

#[derive(Debug, Clone, Copy)]
pub struct ScalarFloatEncoder {
    pub size: usize,
    pub pop: usize,
    pub resolution: f64,
    factor: f64,
}

impl ScalarFloatEncoder {
    pub fn new(size: usize, pop: usize, resolution: f64) -> Self {
        assert!(resolution.is_sign_positive());
        assert!(resolution.is_normal());

        let factor = 1.0 / resolution;
        Self {
            size,
            pop,
            resolution,
            factor,
        }
    }
}

impl ScalarEncoder for ScalarFloatEncoder {
    type Value = f64;

    fn encode(&self, value: Self::Value) -> Sdr {
        let value = if value.is_finite() { value } else { 0.0 };
        let ivalue = (self.factor * value).trunc() as i64;
        let mut npop = 0;
        let mut round = 0;
        let mut ones = HashSet::with_capacity(self.pop);
        while npop < self.pop {
            let v = ivalue + round;
            let v = unsafe { std::mem::transmute(v) };
            let addr = yhash(v) % self.size as u64;
            ones.insert(addr as AddrType);
            npop = ones.len();
            round += 1;
        }

        Sdr::with_uniq_connections(self.size, ones.into_iter().collect())
    }

    fn radius(&self) -> f64 {
        (self.pop as f64) / self.factor
    }
}

/// This isn't really meant to be used outside the crate, but has to be `pub` for example programs to access it.
pub mod util {
    // stolen from https://github.com/eldruin/wyhash-rs
    const P0: u64 = 0xa076_1d64_78bd_642f;
    const P1: u64 = 0xe703_7ed1_a0b4_28db;

    fn wymum(a: u64, b: u64) -> u64 {
        let r = u128::from(a) * u128::from(b);
        ((r >> 64) ^ r) as u64
    }

    // the original "wyrng" mutates its argument, hence the namechange here
    pub fn yhash(val: u64) -> u64 {
        let val = val.wrapping_add(P0);
        wymum(val, val ^ P1)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn overlap_with_fine_resolution() {
        let size = 1000;
        let pop = 20;
        let res = 0.1;
        let enc = ScalarFloatEncoder::new(size, pop, res);

        let one = enc.encode(1.0);
        let one01 = enc.encode(1.01);
        let one1 = enc.encode(1.1);
        let two0 = enc.encode(2.0);
        let three0 = enc.encode(3.0);

        assert_eq!(enc.radius(), 2.0);
        assert_eq!(one.overlap(&one01), pop);
        assert_eq!(one.overlap(&one1), 19);
        assert_eq!(one.overlap(&two0), 11); // "should be" 10, but them's the breaks
        assert_eq!(three0.overlap(&two0), 11); // see above re: "breaks"
    }

    #[test]
    fn overlap_with_coarse_resolution() {
        let size = 1000;
        let pop = 20;
        let res = 1.0;
        let enc = ScalarFloatEncoder::new(size, pop, res);

        let one = enc.encode(1.0);
        let four = enc.encode(4.0);
        let twelve = enc.encode(12.0);
        let twenty_two = enc.encode(22.0);
        let twenty = enc.encode(20.0);

        assert_eq!(enc.radius(), 20.0);
        assert_eq!(one.overlap(&four), 17);
        assert_eq!(one.overlap(&twelve), 9);
        assert_eq!(one.overlap(&twenty_two), 1); // them's the breaks
        assert_eq!(one.overlap(&twenty), 2); // this "should be" 1, but the breaks: they are as they are.
    }
}
