use rand::{
    distributions::Uniform,
    prelude::{Distribution, Rng},
};
use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashSet},
    iter::FromIterator,
};

use crate::tri_mem::ConWeight;

pub const SIZE: usize = 1000;
pub const POP: usize = 20;

/// Used for indexing connections inside SDRs.
pub type AddrType = u16;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Sdr {
    connections: Vec<AddrType>,
    pub size: usize,
}

impl Default for Sdr {
    fn default() -> Self {
        Sdr::new(SIZE, POP)
    }
}

impl Sdr {
    pub fn new(size: usize, pop: usize) -> Self {
        Sdr {
            connections: Vec::with_capacity(pop),
            size,
        }
    }

    pub fn random<R>(size: usize, pop: usize, rng: &mut R) -> Self
    where
        R: Rng,
    {
        let mut set: HashSet<AddrType> = HashSet::with_capacity(pop);

        let range = Uniform::from(0..size as AddrType);
        let mut sz = 0;
        while sz < pop {
            set.insert(range.sample(rng));
            sz = set.len();
        }

        let mut connections: Vec<_> = set.into_iter().collect();
        connections.sort_unstable();

        Sdr { connections, size }
    }

    pub fn with_connections(size: usize, mut connections: Vec<AddrType>) -> Self {
        connections.sort_unstable();
        connections.dedup();
        Sdr { connections, size }
    }

    pub fn with_uniq_connections(size: usize, mut connections: Vec<AddrType>) -> Self {
        connections.sort_unstable();
        Sdr { connections, size }
    }

    /// Constructor from triadic memory connection weights
    pub(crate) fn from_weights(weights: &[ConWeight], pop: usize) -> Self {
        // first, find the "pop"-largest connection weight to use as a threshold value
        // using a heap means we only have to sort `pop` elements instead of the whole thing
        let mut sorted_weights: BinaryHeap<_> = weights.iter().copied().collect();
        let mut ranked_max = 1;
        for _ in 0..pop {
            if let Some(w) = sorted_weights.pop() {
                ranked_max = w;
            } else {
                break;
            }
        }
        ranked_max = ranked_max.max(1);

        // now, walk through the weights and if the value is higher than `ranked_max`, add the index
        // of it to the `connections`.
        let mut connections = Vec::with_capacity(pop);
        for (i, &v) in weights.iter().enumerate() {
            if v >= ranked_max {
                connections.push(i as AddrType);
            }
        }
        Self::with_connections(weights.len(), connections)
    }

    /// "sparse merge"
    pub fn smerge(&self, other: &Self) -> Self {
        assert_eq!(self.size, other.size);

        let size = self.size;

        let mut combo: Vec<_> = self
            .connections
            .iter()
            .chain(other.connections.iter())
            .copied()
            .collect();
        combo.sort_unstable();
        combo.dedup();
        let mut connections: Vec<AddrType> = combo.iter().copied().step_by(2).collect();
        // if we're short on connections, fill from the back of the odds
        if connections.len() < self.pop() {
            for addr in combo[1..].iter().step_by(2).rev() {
                connections.push(*addr);
                if connections.len() >= self.pop() {
                    break;
                }
            }
            connections.sort_unstable();
        }

        Self { connections, size }
    }

    /// "split union"
    pub fn splunion(&self, other: &Self) -> Self {
        assert_eq!(self.size, other.size);

        let size = self.size;

        let s_end = self.pop() / 2;
        let o_start = other.pop() / 2;

        let pop = (self.pop() + other.pop()) / 2;

        let mut combo: HashSet<AddrType> = HashSet::from_iter(
            self.connections[0..s_end]
                .iter()
                .chain(other.connections[o_start..].iter())
                .copied(),
        );

        // backfill any missing connections from the back half of self
        for addr in self.connections[s_end..].iter() {
            if combo.len() >= pop {
                break;
            }
            combo.insert(*addr);
        }

        let mut connections: Vec<AddrType> = combo.into_iter().collect();
        connections.sort_unstable();

        Self { connections, size }
    }

    pub fn union(&self, other: &Self) -> Self {
        let mut connections: Vec<AddrType> = self
            .connections
            .iter()
            .chain(other.connections.iter())
            .copied()
            .collect();
        connections.sort_unstable();
        connections.dedup();

        Sdr {
            connections,
            size: self.size,
        }
    }

    fn distance(&self, other: &Self, zero: usize, incr: isize) -> usize {
        let mut score = zero;

        let mut self_iter = self.connections.iter();
        let mut other_iter = other.connections.iter();

        let mut self_one = self_iter.next();
        let mut other_one = other_iter.next();
        while self_one.is_some() && other_one.is_some() {
            let raw_self_one = self_one.unwrap();
            let raw_other_one = other_one.unwrap();
            match raw_self_one.cmp(raw_other_one) {
                Ordering::Equal => {
                    score = ((score as isize) + incr) as usize;
                    self_one = self_iter.next();
                    other_one = other_iter.next();
                }
                Ordering::Less => {
                    self_one = self_iter.next();
                }
                _ => {
                    other_one = other_iter.next();
                }
            }
        }
        score
    }

    pub fn overlap(&self, other: &Self) -> usize {
        self.distance(other, 0, 1)
    }

    pub fn hamming(&self, other: &Self) -> usize {
        let zero = self.pop() + other.pop();
        self.distance(other, zero, -2)
    }

    pub fn pop(&self) -> usize {
        self.connections.len()
    }

    pub fn connections(&self) -> &[AddrType] {
        &self.connections
    }
}

#[cfg(test)]
mod test {
    //use super::*;

    #[test]
    fn smerge() {}

    #[test]
    fn union() {}
}
